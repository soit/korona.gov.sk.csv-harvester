# Intro

This harvester is intended to maintain copy of data from Covid-19 CSV file
at korona.gov.sk in local PostgreSQL database so that further analysis or
application can be created independently of korona.gov.sk .

This harvester is simple, i.e. it fetches CSV file over and over, taking
some data and pushing updates to database.


# License

This code is licensed under the [EUPL](LICENSE.txt).


# How to install

1. checkout from Git: https://gitlab.com/soit/korona.gov.sk.csv-harvester
2. create virtual environment: `python3 -m venv .venv`
3. activate: `source .venv/bin/activate`
4. install dependencies: `pip install -r requirements.txt`
5. create configuration file: `cp korona_gov_sk_csv_harvester.conf.template korona_gov_sk_csv_harvester.conf`

Then also check:

6. that database exists along with schema 'korona_gov_sk_csv'
7. user running harvester is able to create tables in the database schema
8. configuration in `korona_gov_sk_csv_harvester.conf`, adjust if necessary


# How to run

`python korona_gov_sk_csv_harvester.py`


## be_careful setting

When `True`, harvester will try avoiding overloading either source server
or target database.

Following will be done to avoid overloading source server:

1. server responses will be kept in local SQlite cache
2. after getting data from server, small sleep will be done


## Updates of dataset schema

As of now, code does not handle changes in dataset structure (i.e. DB
schema updates).

If the structure changes, DB (or affected tables) needs to be dropped and
all data downloaded again.

Or DB schema (table names and structure) needs to be updated by hand.


# See also

- visualization: https://opendatask.gitlab.io/viz-covid_19-sk/
