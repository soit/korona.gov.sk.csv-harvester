#!/usr/bin/python3.7
#
# Copyright 2020 by Peter Hanecak <hanecak@opendata.sk>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl 5
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.
#
#
# This harvests and keeps up-to-date statistical data from UPSVAR
# (https://www.upsvr.gov.sk/) in local PostgreeSQL database.
#
# Main points:
# a) on first run creates tables in empty DB and gets all data from korona.gov.sk
# b) on subsequent runs gets updates
# c) one dataset -> one DB table

import configparser
import datetime
import io
import logging
import os
import requests
import requests_cache
import sys
from argparse import ArgumentParser

from git import Repo
from git.exc import GitCommandError

import pandas
from pandas import DataFrame, read_csv

from sqlalchemy import create_engine, inspect
from sqlalchemy import MetaData
from sqlalchemy import Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


DB_SCHEMA = 'korona_gov_sk_csv'

Base = declarative_base(metadata=MetaData(schema=DB_SCHEMA))

class KoronaGovSkCsvHarvester:
    """
    Regarding "data and metadata model" in this harvester:
    
    We're working with one CSV file which contains basic Covid-19 pandemic data for whole Slovakia.
    
    File represents a "rolling update" i.e. we will be adding new items into DB, but in crude way,
    i.e. simply rewriting whatever is in the DB with whatever is in the CSV
    """
    
    CONFIG_FILE_NAME = 'korona_gov_sk_csv_harvester.conf'
    CONFIG_SECTION = 'korona_gov_sk_csv_harvester'
    
    LOCAL_DATASET_FN = 'korona.gov.sk.csv'

    CSV_COLUMN_DATE = 'datum'
    
    DB_TABLE_NAME = 'covid19_stat'
    
    SQLALCHEMY_ECHO = False


    def __init__(self):
        logging.basicConfig(
            format='%(asctime)s %(levelname)s %(message)s',
            level=logging.INFO)
        
        # process config file
        cfg_parser = configparser.ConfigParser()
        cfg_parser.read(self.CONFIG_FILE_NAME)
        self.git_repo_dir = self.git_repo = None
        self.local_dataset_fn = self.LOCAL_DATASET_FN
        self.token = None
        try:
            # mandatory:
            self.cfg_be_careful = cfg_parser.getboolean(self.CONFIG_SECTION, 'be_careful')
            cfg_cache_expire = cfg_parser.getint(self.CONFIG_SECTION, 'cache_expire')
            cfg_db_string = cfg_parser.get(self.CONFIG_SECTION, 'db')
            cfg_log_level = cfg_parser.get(self.CONFIG_SECTION, 'log_level')
            # optional:
            self.cfg_url = cfg_parser.get(self.CONFIG_SECTION, 'url')
            self.git_repo_dir = cfg_parser.get(self.CONFIG_SECTION, 'git_repo', fallback=None)
            self.local_dataset_fn = cfg_parser.get(
                self.CONFIG_SECTION,
                'local_dataset_fn',
                fallback=self.local_dataset_fn)
        except (configparser.NoOptionError, configparser.NoSectionError) as e:
            logging.error('%s (file %s)' % (e, self.CONFIG_FILE_NAME))
            sys.exit(-1)
        
        logging.getLogger().setLevel(cfg_log_level)
        
        # process command-line parameters
        self._parse_cmdln_options()
        
        # initialize the rest (requests, SQLAlchemy)
        self.requests_session = requests.Session()
        if self.cfg_be_careful:
            # to avoid hitting source server too much (mainly during development),
            # we're going to cache all responses for configured amount of time
            self.requests_session = requests_cache.CachedSession(expire_after=cfg_cache_expire)
        
        self.engine = create_engine(cfg_db_string, echo=self.SQLALCHEMY_ECHO)
        self.Session = sessionmaker(bind=self.engine)
        self.session = self.Session()

        self.inspect = inspect(self.engine)

        Base.metadata.create_all(self.engine, checkfirst=True)
        self.metadata = Base.metadata
        self.metadata.bind = self.engine
        self.metadata.reflect(schema=DB_SCHEMA)
        
        if self.git_repo_dir:
            self.git_repo = Repo(self.git_repo_dir)
    
    
    def _commit_to_git(self, data):
        if not self.git_repo:
            return

        fn = os.path.join(self.git_repo_dir, self.local_dataset_fn)
        with open(fn, 'w') as f:
            f.write(data)
        f.close()
        logging.debug('dataset written to %s' % fn)

        something_changed = False
        git = self.git_repo.git
        try:
            git.diff('--quiet')
        except GitCommandError:
            something_changed = True
        if not something_changed:
            return
        git.add(self.local_dataset_fn)
        commit_msg = 'automatic update %s' % datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
        git.commit('-m', commit_msg)
        logging.debug('%s committed into %s: %s' % (self.git_repo_dir, self.local_dataset_fn, commit_msg))
    
    
    def etl_data(self, data):
        """High-level method which:
        1) makes sure we have DB table for given dataset
        2) gets all the data from the dataset and push it into our DB
        3) little bit of tweaking and filtering so that we can account for differences
           between 'chart' and 'map' and pass both for further processing to same methods
        """
        
        date_parser = lambda date: datetime.datetime.strptime(date, '%d-%m-%Y')
        
        f = io.StringIO(data)
        df = read_csv(f, sep=';', header=0, parse_dates=[0], date_parser=date_parser)
        
        # rename columns to lower case, makes it easier to work with in PostreSQL
        rename_cl = {}
        for col_name in df.columns.values:
            rename_cl[col_name] = col_name.lower().replace(' ', '_')
        df.rename(columns=rename_cl, inplace=True)
        
        # index
        df.set_index(self.CSV_COLUMN_DATE, append=False, inplace=True, verify_integrity=True)
        
        # store to DB
        df.to_sql(
            self.DB_TABLE_NAME,
            self.engine,
            schema=DB_SCHEMA,
            if_exists='replace',
            dtype={self.CSV_COLUMN_DATE: Date()}
            )
        self.session.commit()
        
        logging.info('%d rows processed' % df.shape[0])
    
    
    def process_dataset(self, url):
        """Checks whether given dataset needs fetching/update and if so, do it."""

        logging.info('processing dataset: %s' % url)
        r = data = None
        try:
            # fetch data
            logging.info('fetching dataset: URL: %s' % url)
            r = self.requests_session.get(url)
            if r.status_code != 200:
                raise RuntimeError('dataset download failed, status code: %d' % r.status_code)
            data = r.text

            self._commit_to_git(data)

            # do the ETL
            self.etl_data(data)
        except Exception as ex:
            logging.warning('error occurred, rolling back transaction for %s' % url)
            self.session.rollback()
            raise ex
        finally:
            self.session.commit()
    
    
    def _parse_cmdln_options(self):
        parser = ArgumentParser()
        parser.add_argument(
            '-v', '--verbose',
            action="store_true", dest="verbose", default=False,
            help="be more verbose")
        args = parser.parse_args()
        
        if args.verbose:
            logging.getLogger().setLevel('DEBUG')
    
    
    def main(self):
        self.process_dataset(self.cfg_url)


if __name__ == '__main__':
    korona_gov_sk_csv_harvester = KoronaGovSkCsvHarvester()
    korona_gov_sk_csv_harvester.main()
