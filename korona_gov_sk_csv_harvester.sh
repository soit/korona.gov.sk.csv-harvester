#!/bin/bash

LOG=korona_gov_sk_csv_harvester.log

cd `dirname $0`
. .venv/bin/activate

python korona_gov_sk_csv_harvester.py &> $LOG
